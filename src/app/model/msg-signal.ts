export class MsgSignal {
    public id:string;
    public expId:string;
    public signalType:string;
    public dataType:string;
    public destId:string;
    public roomId:string;
   constructor(expId:string,roomId?:string,destId?:string){
    this.expId=expId;
    this.destId=destId;
    this.roomId=roomId;
   }


}