import { Message } from './'
export class Room {
 public id:string;
 public lastMessage: Message;
 public messages:Message[];
 public newMessages:number;
}