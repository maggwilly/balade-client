
export class UserId {
    public   userId:string;
    public   sessionId:string;
    public   userIds:string[];
    public   online:boolean;
    public  lastDate:number;
    constructor(userId:string){
        this.userId=userId;
    }
}