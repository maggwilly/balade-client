import { UserId } from './user';
export class CallSignal {
    public expId:string;
    public signalType:string;
    public destId:string;
    public sdp:any;
    public candidate: any;
   constructor(expId:string,destId?:string){
    this.expId=expId;
    this.destId=destId;
   }
}