
export class Message {
    public id:string;
    public   expId:string;
    public   content:string;
    public   type:string;
    public   roomId:string;
    public   msgType:string;
    public  time:number;
}