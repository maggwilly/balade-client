
// handle s3 uploads
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { MediaSocketService } from '..';

const sliceSize=50000;

@Injectable()
export class AttachmentService {
	constructor(public platform: Platform, public socket: MediaSocketService) {
	}

	public upload(file):Promise<any> {
		let uploadFile=file;
		let self = this;
		return new Promise((resolve, reject) => {
		 self.socket.on(file.name, processing => {
			 if(processing.complete){
				resolve(processing.url)
				return;
			 }
			 if(processing.errorMessage){
				reject(processing.errorMessage)
				return;
			 }
			let place = processing.currentSize;
			let slice = uploadFile.slice(place, place + Math.min(sliceSize, uploadFile.size - place));
			this.readslice(slice,uploadFile);
		})


      this.initUpload(file);
	});
	}
	private initUpload(file:any) {
		let self = this;
		let reader = new FileReader();
		let slice = file.slice(0, sliceSize);
		 reader.readAsArrayBuffer(slice);
		 reader.onload = (event) =>{
		 let arrayBuffer = reader.result;
		 self.socket.emit('file',{
			   name: file.name,
			   type: file.type,
			   size: file.size,
			   data: arrayBuffer
		  });
		};
		reader.onerror =(event) =>{
	   };
   }

	private readslice(slice,uploadFile:any) {
		let self = this;
		let reader = new FileReader();
		reader.readAsArrayBuffer(slice);
		reader.onload = (event)=> {
			let arrayBuffer= reader.result;
		self.socket.emit('file', {
			  name: uploadFile.name,
			  type: uploadFile.type,
			  size: uploadFile.size,
			  data: arrayBuffer 
		 }); 
	   };
   }
}