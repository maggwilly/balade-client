import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio';


@Injectable()
export class AudioService {
	ready = null;
	audio = []
	volume = .4

	constructor(public platform: Platform, public nativeAutio: NativeAudio) {
		this.ready = new Promise((resolve, reject) => {
			platform.ready().then(() => {
				let files = ['message-received-back', 'message-received-front', 'message-sent', 'calling'];
				let c = 1;
				if (this.platform.is('cordova')) {
					files.forEach(file => {
						nativeAutio.preloadComplex(file, 'assets/audio/' + file + '.mp3', this.volume, 1, 0).then(msg => {
							c++;
							if (c === files.length) {
								setTimeout(resolve, 100);
							}
						}, msg => {
							reject('failed to load audio');
						});
					});
				} else {
					files.forEach(file => {
						this.audio[file] = new Audio('assets/audio/' + file + '.mp3');
						this.audio[file].volume = this.volume;
					});
					resolve();
				}
			});
		});

		this.ready.then(() => {
			console.debug('Audio initilized.');
		});
	}

	// play an audio clip
	public play(clip) {
		this.ready.then(() => {
			if (this.platform.is('cordova')) {
				this.nativeAutio.play(clip);
			} else if (this.audio[clip]) {
				this.audio[clip].play();
			}
		});
	}
}
