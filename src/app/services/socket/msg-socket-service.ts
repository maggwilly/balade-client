import { MsgSignal as Signal } from '../../model';
import { Injectable } from '@angular/core';
import { Config } from '../../config';
import * as io from 'socket.io-client';

@Injectable()
export class MsgSocketService {
	socket = null;
	constructor() {
		this.socket = io.connect(Config.msgServer);
		this.socket.on('connect', () => {
			console.log('connected to the Msg-service');
		});

		// triggered when a contact goes offline
		this.socket.on('disconnect', () => {
			console.log('disconnected to the Msg-service');
		});
	}

	// generate a unique custom request id
	private makeId(len) {
		let text = '';
		const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$';
		for (let i = 0; i < (len || 10); i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}

	// send an event and get a response back
	public promise(eventName, signal:Signal) {
		return new Promise((resolve, reject) => {
			const success = response => {
				this.socket.removeListener(signal.dataType);
				resolve(response);
			};
			signal.id = '$response$' + this.makeId(10) + '$';
			this.socket.on(signal.id, success);
			this.socket.emit(eventName, signal);
		});
	}


	public on(name, callback) {
		this.socket.on(name, callback);
	}

	public removeListener(name, data) {
		this.socket.removeListener(name);
	}

	public emit(...args: any[]) {
		this.socket.emit(args[0], args[1])
	}
}