import { Injectable } from '@angular/core';
import { Config } from '../../config';
import * as io from 'socket.io-client';

@Injectable()
export class MediaSocketService {
	socket = null;
	constructor() {
		this.socket = io.connect(Config.msgServer);
		this.socket.on('connect', () => {
			console.log('connected to the Media-service');
		});

		// triggered when a contact goes offline
		this.socket.on('disconnect', () => {
			console.log('disconnected to the Media-service');
		});
	}


	public on(name, callback) {
		this.socket.on(name, callback);
	}

	public removeListener(name, data) {
		this.socket.removeListener(name);
	}

	public emit(...args: any[]) {
		this.socket.emit(args[0], args[1])
	}
}