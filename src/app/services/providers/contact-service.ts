import { Injectable, ApplicationRef } from '@angular/core';
import { MsgSocketService } from '../';
import { MsgSignal as Signal ,UserId } from '../../model';

@Injectable()
export class ContactService {
	friends: UserId[] = [];
	public user: UserId = null
	// manages contacts online status
	constructor(private ref: ApplicationRef,
		 public socket: MsgSocketService) {
		// triggered when a contact comes online
		this.socket.on('online', (contact: UserId) => {
			contact.online = true;
			this.setOnlineUsers([contact]);
		});

		// triggered when a contact goes offline
		this.socket.on('offline', (contact: UserId) => {
			contact.online = false;
			this.setOnlineUsers([contact]);
		});

		// triggered when we request new friends
		this.socket.on('friends', (friends: UserId[]) => {
			this.setOnlineUsers(friends);
		});

	}

	// set online users
	private setOnlineUsers(friends: UserId[]) {
		// i think this happens when user is notified that they are online and we havent recieved the login thing yet
		if (!this.user) {
			console.error('No user yet for contats');
			return;
		}

		for (let x in friends) {
			if (friends[x].userId === this.user.userId) {
				continue;
			}
			let add = true;
			for (let xx in this.friends) {
				if (this.friends[xx].userId === friends[x].userId) {
					// dont overwirte fields with empty data
					this.friends[xx].online = friends[x].online;
					this.friends[xx].sessionId = friends[x].sessionId;
					this.friends[xx].lastDate = friends[x].lastDate;
					add = false;
					break;
				}
			}
			if (add) {
				this.friends.push(friends[x]);
			}
		}
		this.friends.sort(this.sortContacts);
		this.ref.tick();
	};

	// sort contacts by online and last message
	private sortContacts(a: UserId, b: UserId) {
		if (!a) {
			return -1;
		}
		if (!b) {
			return 1;
		}
		if (a.online > b.online) {
			return -1;
		}
		if (a.online < b.online) {
			return 1;
		}
		if (new Date(a.lastDate) > new Date(b.lastDate)) {
			return -1;
		}
		if (new Date(a.lastDate) < new Date(b.lastDate)) {
			return 1;
		}
		return 0;
	};

	// get a contact and its details
	public get(id):UserId {
		if (id === this.user.userId) {
			return this.user;
		}
		for (let contact of this.friends) {
			if (contact.userId === id) {
				return contact;
			}
		}
		return new UserId(id);
	}

	public getContacts() {
		const signal = new Signal(this.user.userId);
		signal.dataType='friends'
		return this.socket.promise('signal', signal);
	}
}