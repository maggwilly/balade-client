import { Injectable, ApplicationRef, NgZone } from '@angular/core';
import { MsgSocketService, RoomService } from '../';
import {UserId,Message, MsgSignal as Signal,Room } from '../../model';

@Injectable()
export class ChatService {
	user:UserId;
	constructor(private zone: NgZone, 
		private ref: ApplicationRef,
		 public socket: MsgSocketService, 
		 public roomService: RoomService) {
		this.socket.on('messages', (room: Room) => {
			this.processMessages(room)
		  });

		// triggered where there is an update to the chat data. like adding a user to the chat
		this.socket.on('new_message', (message: Message) => {
			zone.run(() => {
				for (let r of this.roomService.rooms) {
					if (r.id === message.roomId) {
						this.processMessages(r,[message]);
						return;
					}
				}
			});
		});

	}

	private processMessages(room: Room, msgs?:Message[]) {
		const messages=msgs?msgs:room.messages;
		room.messages.sort(this.sortChats);
		const r:Room = this.roomService.get(room.id);
		  if(!r){
		   this.roomService.addRoom(room);
		   this.ref.tick();
		   return;
	 	}
		for (let message of messages) {
			 r.messages.push(message);
		 }
		this.ref.tick();
	}


	// sort cchats by last message
	private sortChats(a: Message, b: Message) {
		if (!a) {
			return -1;
		}
		if (!b) {
			return 1;
		}
		if (new Date(a.time) > new Date(b.time)) {
			return -1;
		}
		if (new Date(a.time) < new Date(b.time)) {
			return 1;
		}
		return 0;
	};

	// send a new message to another contact
	send(message: Message) {
		this.socket.emit('message', message);
	}

	// @todo: typing
	typing(roomId:string,dataType:string) {
		const signal = new Signal(this.user.userId,roomId);
		signal.dataType=dataType;
		this.socket.emit('signal', signal);
	}

	public getMessages(roomId:string) {
		const signal=new Signal(this.user.userId,roomId);
		signal.dataType='messages'
		return this.socket.promise('signal', signal);
	}

}