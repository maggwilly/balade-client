import { Injectable, ApplicationRef, NgZone } from '@angular/core';
import { MsgSocketService } from '../';
import {Message, MsgSignal as Signal,UserId ,Room} from '../../model';

@Injectable()
export class RoomService {
	rooms: Room[] = []
	user:UserId;
	constructor(private zone: NgZone,
		 private ref: ApplicationRef,
		 private socket: MsgSocketService) {
		// triggered where there is an update to the chat data. like adding a user to the chat
		let self = this;
		this.socket.on('new_message', (message: Message) => {
			let have = false;
			for (let room of this.rooms) {
				if (message.roomId === room.id) {
					have = true;
					break;
				}
			}
			if (!have) {
				self.socket.emit('rooms', { expId: null });
			}
			self.updateLastMessage(message.roomId, message);
		});


		this.socket.on('rooms', (rooms: Room[]) => {
			zone.run(() => {
				// merge new chats to the chat array but dont replace the array itself
				for (let chat of rooms) {
					let i = true;
					for (let c of this.rooms) {
						if (c.id === chat.id) {
							i = false;
							break;
						}
					}
					if (i) {
						this.addRoom(chat);
					}
				}
				this.processChats();
			});
		});
	}

	public addRoom(room:Room){
		this.rooms.push(room);
	}

	public processChats() {
		this.rooms.sort(this.sortChats);
		this.ref.tick();
	}


	// update the last message send for the chats view
	public updateLastMessage(id, message: Message, date = new Date()): void {
		for (let c in this.rooms) {
			let chat = this.rooms[c];
			if (chat.id === id) {
				chat.lastMessage = message;
				chat.newMessages++;
				break;
			}
		}
		this.rooms.sort(this.sortChats);
	}

	public get(id): Room {
		for (let chat of this.rooms) {
			if (chat.id === id) {
				return chat;
			}
		}
		return null;
	}


	// sort cchats by last message
	private sortChats(a: Room, b: Room) {
		if (!a)
			return -1;
		if (!b)
			return 1;
		if (!a.lastMessage)
			return -1;
		if (!b.lastMessage)
			return 1;
		if (new Date(a.lastMessage.time) > new Date(b.lastMessage.time)) {
			return -1;
		}
		if (new Date(a.lastMessage.time) < new Date(b.lastMessage.time)) {
			return 1;
		}
		return 0;
	};

	public getRooms() {
		const signal = new Signal(this.user.userId);
		signal.dataType='rooms'
		return this.socket.promise('signal', signal);
	}
}