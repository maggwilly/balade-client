import {UserId,CallSignal as Signal } from './../../model';
import { Injectable, ApplicationRef } from '@angular/core';
import { CallSocketService,AudioService,ContactService,VideoService} from '../';
import { Platform } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Config } from '../../config';
declare var
	cordova:any,
	window:any,
	RTCSessionDescription:any,
	RTCPeerConnection:any,
	RTCIceCandidate:any;

@Injectable()
export class CallService {
	user:UserId;
	maxTimer = 200000
	facing = 'front'
	pickupTimeout = null
	contact:UserId = null
	isInCall = false
	isCalling = false
	isAnswering = false

	muted = false
	lastState = null
	localStream = null
	peerConnection = null
	remoteVideo = null
	localVideo = null
	peerConnectionConfig = null
	modalShowing = false
	modal = null

	constructor(private ref:ApplicationRef,
		 private sanitizer:DomSanitizer,
		 public socket: CallSocketService,
		 public platform: Platform,
		 private audio: AudioService,
		 public contactService: ContactService,
		 public video: VideoService){
		    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
		    window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
		    window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
		  // STUN/TURN ice servers for connection negotiation
		this.peerConnectionConfig = {'iceServers': Config.ice};
		this.socket.on('callData', (signal:Signal) => {
			switch (signal.signalType) {
				case 'call':
					if (this.isCalling) {
						// we are trying to call eachother. just answer it automaticly
						if (this.contact.userId == signal.destId) {
							clearTimeout(this.pickupTimeout);
							this.pickupTimeout = null;
							this.isCalling = false;
							this.isAnswering = true;
							this.answer();
							return;
						}
						// ignore this incoming call if we are busy
						this.ignore(false);
						return;
					}

					this.audio.play('calling');
					this.pickupTimeout = setTimeout(() => {
						console.log('Call took too long to pick up. Ending.');
						this.end();
					}, this.maxTimer);
					// start a new call
					this.contact = this.contactService.get(signal.destId);
					this.isAnswering = true;
					this.showModal();
					this.preview();
					this.refreshVideos();
					break;

				case 'answer':
					clearTimeout(this.pickupTimeout);
					this.pickupTimeout = null;
					this.isInCall = true;
					this.isCalling = false;
					this.refreshVideos();
					this.call(true);
					break;

				case 'ignore':
				case 'cancel':
					this.end();
					break;
				case 'end':
					if (this.isInCall || this.isCalling || this.isAnswering) {
						this.end();
					}
					break;
				case 'sdp':
					if (signal.sdp) {
						this.peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), () => {
							if (signal.sdp.type === 'offer') {
								this.peerConnection.createAnswer(d => {
									this.gotDescription(d);
								}, e => {
									console.log('error creating answer', e);
								});
							}
						});
					}
					break;
				case 'candidate':
				if (signal.candidate) {
					this.peerConnection.addIceCandidate(new RTCIceCandidate(signal.candidate));
				}
				break;
			}
		});
	}

	public  createCallSignal(dest:UserId,signalType?:string, sdp?:RTCSessionDescription, candidate?:RTCIceCandidate):Signal{
		const signal = new Signal(this.user.userId,dest.userId);
        signal.candidate=candidate;
        signal.sdp=sdp;
        signal.signalType=signalType;
        return signal;
	}

	// place a new call
	public triggerCall(contact:string) {
		if (this.isInCall) {
			return;
		}
		this.audio.play('calling');
		this.showModal();
		this.preview();
		this.pickupTimeout = setTimeout(() => {
			console.log('Call took too long to pick up. Ending.');
			this.end();
		}, this.maxTimer);

		this.contact = this.contactService.get(contact);
		this.isCalling = true;
		this.socket.emit('signal',this.createCallSignal(this.contact,'call'));
	}

	// open the call modal
	showModal(){
		this.modalShowing = true;
	};

	private gotDescription(description:RTCSessionDescription) {
		console.log('got description', description, this.contact);
		this.peerConnection.setLocalDescription(description, () => {
			this.socket.emit('signal', this.createCallSignal(this.contact,'sdp',description,null));
		}, e => {
			console.log('set description error', e)
		});
	}

	private gotIceCandidate(event) {
		if (event.candidate) {
			this.socket.emit('signal', this.createCallSignal(this.contact,'candidate',null,event.candidate));
		}
	}

	private gotRemoteStream(event) {
		console.log('got remote stream');
		this.remoteVideo = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(event.stream));
		this.refreshVideos();
	}

	// a hacky way to make sure we get the latest video position reguardless of animations or transitions
	// another way might be to use iosrtc.observeVideo(video) or an $interval
	refreshVideos() {
		// tell the modal that we need to revresh the video
		this.ref.tick();
		if (!this.platform.is('cordova')) {
			return;
		}
		try {
			for (let x = 0; x <= 3000; x+=300) {
				setTimeout(cordova.plugins.iosrtc.refreshVideos,x);
			}
		} catch (e) {
			console.log(e);
		}
	};

	hideModal() {
		this.modalShowing = false;
	}

	// end the call in either direction
	end() {
		if (this.peerConnection) {
			this.peerConnection.close();
		}

		this.localVideo = null;
		this.remoteVideo = null;
		this.isAnswering = false;
		this.isCalling = false;
		this.isInCall = false;
		this.localStream = null;
		this.video.disconnect().then(() => {
			this.hideModal();
			this.refreshVideos();
		});
		if(!this.contact)
		  return;
		this.socket.emit('signal',  this.createCallSignal(this.contact,'end'));
		this.contact=null;
	}

	// add local stream
	addStream(stream, timeout) {
		this.localStream = stream;
		setTimeout(() => {
			this.localVideo = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(stream));
		}, timeout);
	}

	// preview local video as full screen
	preview() {
		this.video.connect(true, true, this.facing).then(stream => {
			this.addStream(stream, 10);
		});
	};

	// begin a call using webrtc
	call(isInitiator) {
		let connect = () => {
			this.peerConnection = new RTCPeerConnection(this.peerConnectionConfig);
			this.peerConnection.onicecandidate = this.gotIceCandidate.bind(this);
			this.peerConnection.onaddstream = this.gotRemoteStream.bind(this);
			this.peerConnection.oniceconnectionstatechange = event => {
				this.lastState = event.target.iceConnectionState;
				if (this.lastState === 'failed' || this.lastState === 'disconnected' || this.lastState === 'closed') {
					this.peerConnection = null;
					this.end();
				}
			};
			this.peerConnection.addStream(this.localStream);
			if (isInitiator) {
				this.isCalling = true;
				this.peerConnection.createOffer(d => {
					this.gotDescription(d);
				}, e => {
					console.log('error creating offer', e)
				});
			} else {
				this.isAnswering = true;
			}
		};

		if (!this.localStream) {
			this.video.connect(true, true, this.facing).then(stream => {
				this.addStream(stream, 1000);
				connect();
			});
		} else {
			connect();
		}
	}

	// cancel a call being placed
	cancel() {
		this.socket.emit('signal', this.createCallSignal(this.contact,'cancel'));
		this.end();
	};

	// ignore an incomming call
	ignore(end) {
		this.socket.emit('signal',this.createCallSignal(this.contact,'ignore'));
		if (!end) return;
		this.end();
	};

	// answer in incoming call
	answer() {
		if (this.isInCall) {
			return;
		}

		clearTimeout(this.pickupTimeout);
		this.pickupTimeout = null;
		this.isInCall = true;
		this.isAnswering = false;
		this.call(false);
		setTimeout(() => {
			this.socket.emit('signal',this.createCallSignal(this.contact,'answer'));
		},1000);
		this.refreshVideos();
	}

	// swap the camera facing. defaults to front facing to start
	flip() {
		this.facing = this.facing == 'front' ? 'back' : 'front';
		this.video.connect(!this.muted, true, this.facing).then(stream => {
			this.addStream(stream, 0);
			this.peerConnection.addStream(this.localStream);
		});
	}

	// mute the microphone and attach a new stream to connection
	// note: doesnt seem to work quite right on all brwosers
	mute() {
		this.muted = !this.muted;
		if (this.muted) {
			this.video.mute();
		} else {
			this.video.unmute().then(stream => {
				this.addStream(stream, 0);
				this.peerConnection.addStream(this.localStream);
			});
		}
	}
}