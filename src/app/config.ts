
export class Config {
	public static msgServer = 'http://192.168.0.15:5000/';
	public static callServer = 'http://192.168.0.15:5000/';
	public static mediaServer = 'http://192.168.0.15:5000/';
	// STUN/TURN ice servers for connection negotiation
	public static ice = [
		{
			urls: 'stun:stun.l.google.com:19302'
		}, {
			urls: 'stun:stun.services.mozilla.com'
		}, {
			urls: 'stun:numb.viagenie.ca',
			username: 'numb@arzynik.com',
			credential: 'numb'
		}, {
			urls: 'turn:numb.viagenie.ca',
			username: 'numb@arzynik.com',
			credential: 'numb'
		}
	];
	// if we allow attachments or not.
	public static attachments = true;

	// whether or enable markdown parsing client side
	public static markdown = true;
}
